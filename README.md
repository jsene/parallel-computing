## Learning MPI

This repository consists of a collection of small parallel computing projects
that I am completing as I learn more about using MPI, parallel computing, and the
C programming language.
